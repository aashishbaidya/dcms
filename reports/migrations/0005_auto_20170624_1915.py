# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-24 13:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0004_officesetting'),
    ]

    operations = [
        migrations.AddField(
            model_name='karyakram',
            name='name',
            field=models.CharField(default='k', max_length=255, verbose_name='\u0915\u093e\u0930\u094d\u092f\u0915\u094d\u0930\u092e'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='lakxya',
            name='karyakram',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lakxya', to='reports.KaryaKram', verbose_name=' \u0915\u093e\u0930\u094d\u092f\u0915\u094d\u0930\u092e'),
        ),
    ]
