# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-29 11:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0009_officesetting_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fiscalyear',
            name='date_range',
            field=models.CharField(max_length=255, verbose_name='\u0906\u0930\u094d\u0925\u093f\u0915 \u0935\u0930\u094d\u0937'),
        ),
        migrations.AlterField(
            model_name='lakxya',
            name='paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='lakxyahistory',
            name='paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='antarik',
            field=models.FloatField(default=0.0, verbose_name='\u0905\u0928\u094d\u0924\u0930\u093f\u0915'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='baidesikh',
            field=models.FloatField(default=0.0, verbose_name='\u0935\u0948\u0926\u0947\u0936\u093f\u0915'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='budget_rs',
            field=models.FloatField(default=0.0, verbose_name='\u092c\u091c\u0947\u091f'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='budget_year',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='fiscal_year', to='reports.FiscalYear', verbose_name='\u0906\u0930\u094d\u0925\u093f\u0915 \u0935\u0930\u094d\u0937'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='dritasastha',
            field=models.FloatField(default=0.0, verbose_name='\u0926\u093e\u0924\u0943\u0938\u0902\u0938\u094d\u0925\u093e'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='janasamglanta',
            field=models.FloatField(default=0.0, verbose_name='\u091c\u0928\u0938\u0902\u0917\u094d\u0932\u0928\u0924\u093e'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='loan',
            field=models.FloatField(default=0.0, verbose_name='\u090b\u0923'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='nepalsarkar',
            field=models.FloatField(default=0.0, verbose_name='\u0928\u0947\u092a\u093e\u0932 \u0938\u0930\u0915\u093e\u0930'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='sastha',
            field=models.FloatField(default=0.0, verbose_name='\u0938\u0902\u0938\u094d\u0925\u093e'),
        ),
        migrations.AlterField(
            model_name='officebudget',
            name='upadan',
            field=models.FloatField(default=0.0, verbose_name='\u0909\u092a\u0926\u093e\u0928'),
        ),
        migrations.AlterField(
            model_name='pragati',
            name='abp_paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='pragati',
            name='cmh_paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='pragati',
            name='paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='pragati',
            name='pas_paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
        migrations.AlterField(
            model_name='pragatihistory',
            name='paridam',
            field=models.FloatField(default=0.0, verbose_name='\u092a\u0930\u093f\u092e\u093e\u0923'),
        ),
    ]
